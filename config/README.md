# CONFIG

## Live template

The file named 'config_TEMPLATE.json' is a template configuration file.
It should be copied to the config/live/ directory and changed to config.json.   
Any live configuration changes will be done here.  WARNING: it will not be 
pushed to git so keep it safe.

