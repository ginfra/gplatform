
## IMPORTANT NOTES

These files were for setting up my test environment.  It was a number
of VMs on a hefty windows box.  Why windows?  Because when I had this project off
for it's intended initial use it will be to someone that has the same hardware.
Feel free to use this or don't. 

Some of these scripts rely on the config.sh file.  Use the template provided 
to create one.  It will not be pushed to git.

## VM setup and management.

My VMs are always debian.   For instance, if I had a proper DNS server for my 
entire network, I wouldn't create one here.

# The network I used

I reserved 192.168.1.220 through 192.168.1.220 for my cluster.  

192.168.1.220 is the commandcenter and node 1
192.168.1.222 is node 2

# DNS server

The script sets up a dns server.  It will use the bind config files in the bind/ 
directory.  Those files are set up for my test cluster, so you will have to 
customize them to your cluster.  To use these scripts you 

# Go

If you plan on using one of the nodes to build ginfra, you'll need to install go on it.
The install_go.sh script installs the current version used for testing.

# Docker repository

If you need a local docker registry, you can run the install_docker_registry.sh script.
Generally this is not a good idea for production.

