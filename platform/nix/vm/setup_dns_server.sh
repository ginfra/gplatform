#!/usr/bin/env bash

SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
source "${SCRIPT_DIR}/config.sh"

apt update
apt install -y bind9 bind9utils bind9-doc

# Set just IPV4 for simplicity.  The cluster will never get bigger than that.
echo 'OPTIONS="-u bind -4"' > /etc/default/bind9
echo "" >> /etc/default/bind9

cp -R ${GINFRA_HOME}/platform/nix/nodes/vm/bind/* /etc/bind

systemctl restart bind9

${GINFRA_HOME}/platform/nix/nodes/setup_dns_client.sh

echo "Done setting up DNS server."
