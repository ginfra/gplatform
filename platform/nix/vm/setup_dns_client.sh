#!/usr/bin/env bash

SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
source "${SCRIPT_DIR}/config.sh"

cp -f /etc/resolv.conf /etc/resolv.conf_predns
echo "domain ${GI_DOMAIN}" > /etc/resolv.conf
echo "search ${GI_DOMAIN}." >> /etc/resolv.conf
echo "nameserver ${GI_DNS_SERVER_IP}" >> /etc/resolv.conf

echo "Done setting up DNS client."
