
## AT LOGIN

This assumes shared folder 'dev' is a VirtualBox shared folder name that 
points to the directory where you have cloned your repos.  If these seems right, 
you might want to put it in your /etc/fstab (or for whatever shell you use).

    mkdir /develop
    chmod 777 /develop
    mount -t vboxsf dev /develop

This would be a good time to set export GINFRA_HOME and put it into your .profile
for future logins.

    export GINFRA_HOME="/develop/ginfra"

