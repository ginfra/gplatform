#!/usr/bin/env bash

cd /tmp
wget https://go.dev/dl/go1.20.6.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.20.6.linux-amd64.tar.gz

echo "" >> /etc/profile
echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile
# This is normally worthless, but there are some cases when different users confuse go install.
echo "" >> /etc/profile

rm -f go1.20.6.linux-amd64.tar.gz
