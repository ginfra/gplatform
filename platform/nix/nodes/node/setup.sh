#!/usr/bin/env bash

. ./config.sh

apt install -y sudo
sudo adduser ${GUSER_NAME} sudo
echo ${GHOST_NAME} > /etc/hostname

cp /etc/hosts /etc/hosts.BAK

echo "127.0.0.1	localhost" > /etc/hosts
echo "127.0.1.1	${GHOST_NAME}" >> /etc/hosts
echo "::1     localhost ip6-localhost ip6-loopback" >> /etc/hosts
echo "ff02::1 ip6-allnodes" >> /etc/hosts
echo "${GHOST_IP}	${GHOST_NAME}" >> /etc/hosts

systemctl disable apparmor


