## Initial setup

You'll have to do this by hand, since the scripts aren't on the machine yet.

AS ROOT:
```
apt update
apt install -y git

cd /
mkdir /build
chmod 777 /build
```

AS YOUR USER (mine is 'gi')
```
cd /build
git clone https://gitlab.com/ginfra/gplatform.git
```

AS ROOT:
```
cd /build/gplatform/platform/nix/nodes/node
./prep.sh
```

It will tell you to edit config.sh.

AS ROOT:
```
./setup.sh
```

You should reboot before doing anything else.
