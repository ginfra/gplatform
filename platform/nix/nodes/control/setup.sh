#!/usr/bin/env bash

wget https://go.dev/dl/go1.23.0.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.23.0.linux-amd64.tar.gz
rm -f go1.23.0.linux-amd64.tar.gz

cd
mkdir -p go/bin
NEWPATH='"$PATH:/usr/local/go/bin:$PWD/go/bin"'
echo "PATH=${NEWPATH}" >> .profile
PATH="$PATH:/usr/local/go/bin:$PWD/go/bin"

mkdir /build
chmod 777 /build

apt-get -y install buildah

