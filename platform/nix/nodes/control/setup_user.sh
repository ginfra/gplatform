#!/usr/bin/env bash

cd
mkdir -p go/bin
NEWPATH='"$PATH:/usr/local/go/bin:$PWD/go/bin"'
echo "PATH=${NEWPATH}" >> .profile
PATH="$PATH:/usr/local/go/bin:$PWD/go/bin"

go install github.com/go-task/task/v3/cmd/task@latest
sudo cp go/bin/task /usr/local/bin

cd /build
git clone https://gitlab.com/ginfra/ginfra.git
git clone https://gitlab.com/ginfra/gservices.git
echo "export GINFRA_HOME=/build/ginfra" >> .profile
