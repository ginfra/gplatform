## Initial setup

This assumes the node/ setup is already done.

RUN AS ROOT:
```
./setup.sh
```

RUN AS YOUR ADMIN USER:
```
./setup_user.sh
```

