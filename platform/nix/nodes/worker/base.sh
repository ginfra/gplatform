#!/usr/bin/env bash

# GINFRA
SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
source ../../../../bin/set_ginfra_home.sh
source "${GINFRA_HOME}/config/static.sh"

# Install packages
apt-get update && \
    apt-get install -y vim

# Install Docker
install -m 0755 -d /etc/apt/keyrings && \
    apt-get install -y curl gpg && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    chmod a+r /etc/apt/keyrings/docker.gpg && \
    echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null  && \
    apt-get update && \
    apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
echo "Docker installed"

# Install Docker CRI
cd /tmp
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${GI_CRI_VERSION}/cri-dockerd-${GI_CRI_VERSION}.amd64.tgz && \
    tar xzf cri-dockerd-${GI_CRI_VERSION}.amd64.tgz && \
    cd cri-dockerd && \
    mkdir -p /usr/local/bin && \
    install -o root -g root -m 0755 cri-dockerd /usr/local/bin/cri-dockerd && \
    wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.service && \
    wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket && \
    mv cri-docker.socket cri-docker.service /etc/systemd/system/ && \
    sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service && \
    systemctl daemon-reload  && \
    systemctl enable cri-docker.service && \
    systemctl enable --now cri-docker.socket

cd /tmp
rm -rf cri-dockerd-${GI_CRI_VERSION}.amd64.tgz cri-dockerd

if systemctl status cri-docker.socket | grep 'active (listening)' ; then
    echo "CRI running."
else
    echo "ERROR: CRI not running.  Giving up." ;
    exit 9
fi

# Install Kubernetes bits
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
apt update
apt install -y kubeadm kubelet kubectl
apt-mark hold kubeadm kubelet kubectl


echo "Done base"
