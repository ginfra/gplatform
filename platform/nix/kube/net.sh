#!/usr/bin/env bash

# Calico CNI
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.1/manifests/tigera-operator.yaml
curl https://raw.githubusercontent.com/projectcalico/calico/v3.28.1/manifests/custom-resources.yaml -O
sed -i -e "s|192.168.0.0/16|192.168.2.0/24|g" custom-resources.yaml
kubectl create -f custom-resources.yaml

curl -L https://github.com/projectcalico/calico/releases/download/v3.28.1/calicoctl-linux-amd64 -o calicoctl
chmod +x ./calicoctl
mv calicoctl /usr/local/bin/

