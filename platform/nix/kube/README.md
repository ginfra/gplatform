# Kubernetes support

### Prerequisites

This assumes platform/nix/nodes/node/ setup has been done.

If this instance is a primary control plane node, also do platform/nix/nodes/control/

### Install and setup

RUN AS ROOT  :
You won't have to use root after this.

- Run ./prep.sh 
- Edit the config.sh to set the admin user you plan to use when using kubeadm.
- Run ./setup.sh
- Run ./install.sh
- If it is the primary control plane node, run ./primary.sh.  Note the output will tell you how to add
  other nodes to the cluster.  After a brief wait, run  ./net.sh
- If not the primary control plane node, add it to the cluster.  When you created the cluster, you would have seen 
  some output like this.
```
Then you can join any number of worker nodes by running the following on each as root:
kubeadm join node1cp:6443 --token rzqpgg.de9xtxwfy39j109y \
--discovery-token-ca-cert-hash sha256:837c12922b81514fdf2a6d03914b7ab5c91f327f9eb2689639b8cd5b5b2695f2 
```

RUN AS YOUR ADMIN USER (mine is 'gi'):
- Run ./post
- Run the following (if you plan on using the Ginfra application and the supporting services:
```
cd /build
git clone https://gitlab.com/ginfra/ginfra.git
git clone https://gitlab.com/ginfra/gservices.git
```

### User

Log in and out of the root account before doing anything else.

### Add workers

The output from the primary.sh command will tell you how to add workers.  (The workers themselves will
need to go through this install process too.)

### Setup network

kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml

Check it out with: kubectl get pods -n kube-system

At this point it should be ready.  You can verify with: kubectl get nodes

