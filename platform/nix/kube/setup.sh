#!/usr/bin/env bash

swapoff -a
echo "Make sure you disable any swap partition at startup."

. ./config.sh

sudo adduser ${KUBE_USER} sudo


