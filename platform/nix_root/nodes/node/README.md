## Initial setup

This is a ROOT ONLY install.  Everything must be done as the root use.  DO NOT use for production!

You'll have to do this by hand, since the scripts aren't on the machine yet.

AS ROOT:
```
apt update
apt install -y git

cd /
mkdir /build
chmod 777 /build
cd /build
git clone https://gitlab.com/ginfra/gplatform.git
cd /build/gplatform/platform/nix/nodes/node
./setup.sh
```
