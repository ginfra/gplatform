#!/usr/bin/env bash

# Usage
# ARG1: hostname
# ARG2: ip

echo ${1} > /etc/hostname

cp /etc/hosts /etc/hosts.BAK

echo "127.0.0.1	localhost" > /etc/hosts
echo "127.0.1.1	${1}" >> /etc/hosts
echo "::1     localhost ip6-localhost ip6-loopback" >> /etc/hosts
echo "ff02::1 ip6-allnodes" >> /etc/hosts
echo "${2}	${1}" >> /etc/hosts

systemctl disable apparmor


