#!/usr/bin/env bash

wget https://go.dev/dl/go1.23.0.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.23.0.linux-amd64.tar.gz
rm -f go1.23.0.linux-amd64.tar.gz

cd
mkdir -p go/bin
NEWPATH='"$PATH:/usr/local/go/bin:$PWD/go/bin"'
echo "PATH=${NEWPATH}" >> .profile
PATH="$PATH:/usr/local/go/bin:$PWD/go/bin"

go install github.com/go-task/task/v3/cmd/task@latest
cp go/bin/task /usr/local/bin

apt-get -y install buildah

cd /build
git clone https://gitlab.com/ginfra/ginfra.git
git clone https://gitlab.com/ginfra/gservices.git
echo "export GINFRA_HOME=/build/ginfra" >> ~/.profile
