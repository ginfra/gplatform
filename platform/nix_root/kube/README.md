# Kubernetes support

### Root only

This is a ROOT ONLY install.  Everything must be done as the root use.  DO NOT use for production!

### Install and setup

RUN AS ROOT  :
- Run the following to get this repo:
```
apt update
apt install -y git

cd /
mkdir /build
chmod 777 /build
cd /build
git clone https://gitlab.com/ginfra/gplatform.git
cd gplatform/platform/nix_root/kube/
```
- Run ./setup.sh [desired hostname] [host ip] 
- Reboot
- Run ./install.sh
- If it is the primary control plane node, run ./primary.sh.
- If not the primary control plane node, add it to the cluster.  When you created the cluster, you would have seen 
  some output like this.
```
Then you can join any number of worker nodes by running the following on each as root:
kubeadm join node1cp:6443 --token rzqpgg.de9xtxwfy39j109y \
--discovery-token-ca-cert-hash sha256:837c12922b81514fdf2a6d03914b7ab5c91f327f9eb2689639b8cd5b5b2695f2 
```

You can use this to add workers.

### Worker nodes

The above instructions work for the worker nodes too, just don't run the ./primary.sh script.

### Network

Unless you have set up DNS, make sure all the nodes in your cluster are in the /etc/hosts file on every node.
