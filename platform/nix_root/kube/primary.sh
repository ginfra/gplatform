#!/usr/bin/env bash

GHOST_NAME=`cat /etc/hostname`

cp assets/kubelet_init.yaml.TPL kubelet_init.yaml
sed -i -e "s/__GHOST__/${GHOST_NAME}/g" kubelet_init.yaml

kubeadm init --config kubelet_init.yaml

export KUBECONFIG=/etc/kubernetes/admin.conf
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> ~/.profile

# Calico CNI
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.28.1/manifests/tigera-operator.yaml
curl https://raw.githubusercontent.com/projectcalico/calico/v3.28.1/manifests/custom-resources.yaml -O
sed -i -e "s|192.168.0.0/16|192.168.2.0/24|g" custom-resources.yaml
kubectl create -f custom-resources.yaml

curl -L https://github.com/projectcalico/calico/releases/download/v3.28.1/calicoctl-linux-amd64 -o calicoctl
chmod +x ./calicoctl
mv calicoctl /usr/local/bin/

../nodes/control/setup.sh

cd
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config














