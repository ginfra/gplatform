#!/usr/bin/env bash

swapoff -a
echo "Make sure you disable any swap partition at startup."

# Prep
cat <<EOF | tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
modprobe overlay
modprobe br_netfilter
cat <<EOF | tee /etc/sysctl.d/99-kubernetes-k8s.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sysctl --system

# ContainerD
apt update
apt -y install containerd gpg curl make uidmap
apt -y install containernetworking-plugins

containerd config default | tee /etc/containerd/config.toml >/dev/null 2>&1
sed -i -e 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
sed -i -e 's/disable_apparmor = false/disable_apparmor = true/g' /etc/containerd/config.toml
sed -i -e 's/restrict_oom_score_adj = false/restrict_oom_score_adj = true/g' /etc/containerd/config.toml
systemctl restart containerd
systemctl enable containerd

# I don't have time to fight this.
sed -i -e 's/example.com/docker.io/g' /etc/containers/registries.conf
sed -i -e 's/# unqualified-search/unqualified-search/g' /etc/containers/registries.conf

# Helm - from source.  The packages are a bit squirrelly.
cd /tmp
wget https://get.helm.sh/helm-v3.15.4-linux-amd64.tar.gz
tar xzf helm-v3.15.4-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin
rm -rf linux-amd64 helm-v3.15.4-linux-amd64.tar.gz

# Nerdctl
wget https://github.com/containerd/nerdctl/releases/download/v1.7.6/nerdctl-full-1.7.6-linux-amd64.tar.gz
tar Cxzvvf /usr/local nerdctl-full-1.7.6-linux-amd64.tar.gz
rm -f nerdctl-full-1.7.6-linux-amd64.tar.gz

# Kubernetes
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list

apt update &&
  apt install kubelet kubeadm kubectl -y &&
  apt-mark hold kubelet kubeadm kubectl


