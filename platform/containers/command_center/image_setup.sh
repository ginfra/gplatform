#!/usr/bin/env bash

apt-get update && apt-get -y install \
  apt-utils \
  build-essential \
  curl \
  doxygen \
  git \
  tar \
  wget \
  zip \
  automake \
  libtool \
  libudev-dev \
  ninja-build python-dev


