#!/usr/bin/env bash

GINFRA_DIR=$(dirname $( dirname -- "$( readlink -f -- "$0"; )"; ) )

if ! grep -q GINFRA_HOME /etc/profile ; then
  echo "" >> /etc/profile
  echo "export GINFRA_HOME=${GINFRA_DIR}" >> /etc/profile
  echo "export PATH=$PATH:$GINFRA_HOME/bin"  >> /etc/profile
  echo "" >> /etc/profile
  # In  case it is a setup script sourcing this.
  export GINFRA_HOME=${GINFRA_DIR}
  export PATH=$PATH:$GINFRA_HOME/bin
fi
