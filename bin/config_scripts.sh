#!/usr/bin/env bash

while read -r config_item
do
    DIRECTIVE=$( echo "${config_item}" | cut -d ":" -f 1 )
    if [ "${DIRECTIVE}" == "PATH" ] ; then
        NAME=$( echo "${config_item}" | cut -d ":" -f 2 )
        VALUE=$( echo "${config_item}" | cut -d ":" -f 3 )
        export ${NAME}=${GINFRA_HOME}/${VALUE}
    fi

done < ${GINFRA_HOME}/config/internal/config.txt

