#!/usr/bin/env bash

START_DIR=$PWD
cd
ROOT_DIR=$PWD

go env -w GOBIN=${ROOT_DIR}/go/bin
echo "" >> ${ROOT_DIR}/.profile
echo "PATH=${PATH}:${ROOT_DIR}/go/bin" >> ${ROOT_DIR}/.profile
echo "" >> ${ROOT_DIR}/.profile

cd $START_DIR

echo "Log out and then back in for all settings to take effect."
