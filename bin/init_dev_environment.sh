#!/usr/bin/env bash

START_DIR=$PWD

cd /tmp
wget https://go.dev/dl/go1.20.6.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.20.6.linux-amd64.tar.gz

echo "" >> /etc/profile
echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile
# This is normally worthless, but there are some cases when different users confuse go install.
echo "" >> /etc/profile

rm -f go1.20.6.linux-amd64.tar.gz

apt-get update && apt-get install -y unzip

go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest

cd /tmp
wget https://github.com/mikefarah/yq/releases/download/v4.34.2/yq_linux_amd64.tar.gz -O - |\
  tar xz && mv yq_linux_amd64 /usr/bin/yq
./install-man-page.sh
rm -f install-man-page.sh yq.1

sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d
mv bin/task /usr/local/bin/
chmod 755  /usr/local/bin/task
rm -rf bin

curl -fsSL https://quobix.com/scripts/install_vacuum.sh | sh

go install honnef.co/go/tools/cmd/staticcheck@latest

cd "${START_DIR}"

